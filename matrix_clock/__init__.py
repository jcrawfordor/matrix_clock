import configparser
import time
from rgbmatrix import RGBMatrix, RGBMatrixOptions, graphics

def run():
    config = configparser.ConfigParser()
    config.read('config.ini')

    options = RGBMatrixOptions()
    options.hardware_mapping = config['matrix']['mapping']
    options.rows = int(config['matrix']['rows'])
    options.cols = int(config['matrix']['cols'])
    options.chain_length = int(config['matrix']['chain'])

    matrix = RGBMatrix(options=options)

    print("Finished matrix setup, starting loop.")

    canvas = matrix.CreateFrameCanvas()
    font = graphics.Font()
    font.LoadFont("fonts/envypn7x15.bdf")
    textColor = graphics.Color(255,255,0)
    text = "Test"

    while True:
        canvas.Clear()
        len = graphics.DrawText(canvas, font, 10, 15, textColor, text)
        time.sleep(0.05)
        canvas = matrix.SwapOnVSync(canvas)
